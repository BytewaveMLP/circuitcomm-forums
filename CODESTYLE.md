Codestyle Documentation
=======================

## Overview

Here at *Kasaract Studios*, we strive to make our code as readable as possible, and make edits a breeze. With our standardized codestyle documentation for all of our projects, our code is easily edited and modified to your preference.

## Indentation

### PHP

When creating loops or functions in PHP, always indent by *four spaces* after the opening tag.

```php
<?php
function func( $arg ) {
	doStuffWithArg( $arg );
}
?>
```

```
<?php
if ( $one == $two ) {
	doStuff();
}
?>
```

### HTML/CSS

When opening a new tag or defining a new rule, always make sure to indent four spaces on a new line.

```html
<html>
	<head>
		...
	</head>
</html>
```

```css
.footer {
	font-size: 100px;
}
```

### Exceptions

#### HTML

For one-op statements such as parts to a dropdown or navbar from Bootstrap, or for style-based tags, no indentation/newlines may be used.

```html
<li><a href="#">Return to #</a></li>
```

### PHP

For mid-code operations, single line statements may be used.

```php
<p>Welcome, <?php echo $username; ?></p>
```

## HTML5

### DOCTYPE

For Bootstrap to work properly, the HTML5 DOCTYPE must be declared properly.

```html
<!DOCTYPE html>
<html>
	<head>
		...
	</head>
	<body>
		...
	</body>
</html>
```

### Charset

To ensure proper standardization, please use the UTF-8 charset in your code.

```html
...
<head>
	<meta charset="utf-8">
</head>
...
```

### Lang attribute

*PLEASE* define the lang attribute (based on the user's choice in this case) in your code! This makes browsers with translation capabilities recognize the page as foreign for a user much faster!

```html
<html lang="en-US">
	...
</html>
```

### Meta tags

#### Viewport

For Bootstrap projects, please make sure to define the device viewport tag.

```html
...
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
...
```

To prevent touch zoom (*Not* recommended), use this viewport tag.
```html
...
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
...
```

#### Description/author

Define the description and/or author of a page by either the users' settings or by autogeneration- or as descriptive as possible. The definition of this tag's value should be based on what the content actually is.

```html
...
<head>
	<meta name="description" content="">
	<meta name="author" content="">
</head>
...
```

## Spacing

### PHP

Please use spaces wherever possible! (See examples for explanation)

```php
<?php
function useSpaces( $arg ) {
	doSomething( $arg2 );
}
?>
```

```php
<?php
if ( 1 == 2 ) {
	echo "Yay!";
}
?>
```

### CSS

Use spaces. Period. See example.

```css
p .head .two {
	font-family: Arial;
}
```

## Commenting

### HTML

#### Inclusion

For including files, state what you are including when you include them.

```html
...
<!-- Bootstrap core JavaScript -->
<script src="inc/js/jquery-1.10.2.js"></script>
<script src="inc/js/bootstrap.js"></script>
...
```

#### Closing tags

When closing a tag in HTML, please dictate what class that tag currently has. This allows for easy code browsing for long series of classes such as panels in HTML.

```html
...
<div class="container">
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Panel</h3>
			</div> <!-- /.panel-heading -->
			<div class="panel-body">
				Cotnent
			</div> <!-- /.panel-body -->
		</div> <!-- /.panel .panel-primary -->
	</div> <!-- /.col-md-6 -->
</div> <!-- /.container -->
```

### PHP

When defining functions/classes, always dictate what they do. Comment on what you're doing at every step in the way if it's even slightly confusing. Make sure you document it in an [ApiGen](http://apigen.org/) friendly [manner](http://www.sitepoint.com/generate-documentation-with-apigen/).

```php
<?php
/**
 * The User class is a sample class that holds a single
 * user instance.
 *
 * The constructor of the User class takes a Data Mapper which
 * handles the user persistence in the database. In this case,
 * we will provide a fake one.
 *
 * @category  MyLibrary
 * @package   Mane
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @example   ../index.php
 * @example <br />
 *  $oUser = new MyLibraryUser(new MappersUserMapper());<br />
 *  $oUser->setUsername('swader');<br />
 *  $aAllEmails = $oUser->getEmails();<br />
 *  $oUser->addEmail('test@test.com');<br />
 * @version   0.01
 * @since     2012-07-07
 * @author    John Doe <john-doe@example.com>
 */

class User {
	...
}
?>
```