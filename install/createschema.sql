-- NOTE: Not using UTF-8 due to *easy* injection exploits. Will disucss later.)

DROP TABLE IF EXISTS ccomm_user_settings;
DROP TABLE IF EXISTS ccomm_user;

CREATE TABLE ccomm_user
(
	`user_id` int AUTO_INCREMENT,
	`username` varchar(30) UNIQUE,
	`password` text NOT NULL,
	`avatar_url` text,
	`gender` enum('m','f','n') NOT NULL,
	`bio` longtext,
	`email` text NOT NULL,
	`registered` enum('y','n') NOT NULL,
	`pass_salt` text NOT NULL,
	`group_id` int,
	PRIMARY KEY (`user_id`)
);

CREATE TABLE ccom_user_settings
(
	`user_id` int,
	PRIMARY KEY (`user_id`),
	FOREIGN KEY (`user_id`) REFERENCES ccomm_user(`user_id`)
);