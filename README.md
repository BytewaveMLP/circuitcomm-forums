CircuitComm-Forums
==================

## Overview

Do you own a gaming community?

Are you in need of forum software?

Do you want something totally *amazing*?

Welcome to **CircuitComm**, a forum software developed for the massive number of gaming communities and typical users alike.

Here at Kasaract Studios, we strive to develop quality software, which is easily used and understood by the end user (yourself).

CircuitComm is developed with the intent of being brilliant, and strives to be an excellent forum software.

CircuitComm is:

-	Coded from near scratch (we only use Twitter Bootstrap and a few templates)
-	Home-brewed by two developers
-	Minute
-	OCD approved
-	Glorious

## Information

CircuitComm is a revolutionary new forum software built around gaming communities- specifically those of the Steam network.

CircuitComm attempts to unify your server(s) with your forums, utilizing utilities such as web APIs and such to transfer information back and forth between your forums and your servers, allowing for easy synchronization and optimization.

CircuitComm attempts to also push the boundaries of forum software. After careful consideration, the Kasaract Studios team has decided upon using [Markdown](http://daringfireball.net/projects/markdown/) (using the [Parsedown](http://parsedown.org/) PHP library) as our styling system- as well as [Markdown Extra](https://michelf.ca/projects/php-markdown/extra/). It may not be the best in the world, but it's a lot more user friendly. *Basic* HTML- such as `<span>` for styling text further, will be allowed. However, security measures will be taken against malicious tags such as `<iframe>` or `<script>`.

CircuitComm also plans to implement GeSHi syntax highlighting for code tags. This will make forums with certain code look nicer and sleek. Code types will be defined in a GitHub-Flavored Markdown manner.

## Installation

**_Not currently available_**

## Download

**_Not currently available_**
